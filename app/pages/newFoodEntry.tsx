import { BlitzPage } from "blitz"
import FoodEntryForm from "app/core/components/FoodEntryForm"
import DashboardLayout from "app/core/layouts/DashboardLayout"

const NewFoodEntry: BlitzPage = () => {
  return <FoodEntryForm />
}

NewFoodEntry.suppressFirstRenderFlicker = true

NewFoodEntry.getLayout = (page) => {
  return <DashboardLayout title="Meals">{page}</DashboardLayout>
}

export default NewFoodEntry
