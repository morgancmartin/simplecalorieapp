import { Flex, Button } from "@chakra-ui/react"
import { useEffect, useState } from "react"
import { BlitzPage } from "blitz"
import CalorieTotals from "app/core/components/CalorieTotals"
import Layout from "app/core/layouts/Layout"
import DashboardLayout from "app/core/layouts/DashboardLayout"

const CalorieTotalsPage: BlitzPage = () => {
  return <CalorieTotals />
}

CalorieTotalsPage.suppressFirstRenderFlicker = true
CalorieTotalsPage.getLayout = (page) => {
  return <DashboardLayout title="Meals">{page}</DashboardLayout>
}

export default CalorieTotalsPage
