import { Flex, Button } from "@chakra-ui/react"
import { useEffect, useState } from "react"
import { BlitzPage } from "blitz"
import { useCurrentUser } from "app/core/hooks/useCurrentUser"
import MealForm from "app/core/components/MealForm"
import DashboardLayout from "app/core/layouts/DashboardLayout"

const NewMeal: BlitzPage = () => {
  return <MealForm />
}

NewMeal.suppressFirstRenderFlicker = true

NewMeal.getLayout = (page) => {
  return <DashboardLayout title="Meals">{page}</DashboardLayout>
}

export default NewMeal
