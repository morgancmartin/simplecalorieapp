import { Flex } from "@chakra-ui/react"
import { useEffect, useState } from "react"
import { BlitzPage } from "blitz"
import DashboardLayout from "app/core/layouts/DashboardLayout"
import AddedEntriesStats from "app/core/components/AddedEntriesStats"
import CalorieTotals from "app/core/components/CalorieTotals"
import moment from "moment"
import { useQuery } from "blitz"
import getFoodEntries from "app/foodEntries/queries/getFoodEntries"
import { groupBy, sum, mean } from "lodash"

const Report: BlitzPage = () => {
  const [foodEntries, { refetch }] = useQuery(getFoodEntries, {})
  const stats = calculateEntriesStats(foodEntries)

  return (
    <Flex direction="column" width="80%">
      <Flex height="30vh">
        <AddedEntriesStats {...stats} />
      </Flex>
      <CalorieTotals />
    </Flex>
  )
}

Report.suppressFirstRenderFlicker = true

Report.getLayout = (page) => {
  return <DashboardLayout title="Meals">{page}</DashboardLayout>
}

function calculateEntriesStats(foodEntries) {
  const today = foodEntries.filter((entry) =>
    moment(entry.consumptionDate).startOf("day").isSame(moment().startOf("day"))
  ).length

  const pastSevenDays = entriesInPastSevenDays(foodEntries).length

  const weekBefore = foodEntries.filter(
    (entry) =>
      moment(entry.consumptionDate)
        .startOf("day")
        .isSameOrAfter(moment().startOf("day").subtract(14, "days")) &&
      moment(entry.consumptionDate)
        .startOf("day")
        .isBefore(moment().startOf("day").subtract(7, "days"))
  ).length

  return {
    today,
    pastSevenDays,
    weekBefore,
  }
}

function calculateAvgCalsStats(foodEntries) {
  const pastSevenDayEntries = entriesInPastSevenDays(foodEntries)
  const entriesByUser = groupBy(pastSevenDayEntries, "userId")
  const totalCalsPerUser = Object.entries(entriesByUser).map((entry) =>
    sum(entry[1].map((foodEntry) => foodEntry.calories))
  )

  return {
    avgCalsPastSevenDays: mean(totalCalsPerUser),
  }
}

function entriesInPastSevenDays(foodEntries) {
  return foodEntries.filter((entry) =>
    moment(entry.consumptionDate)
      .startOf("day")
      .isSameOrAfter(moment().startOf("day").subtract(7, "days"))
  )
}

export default Report
