import { Flex, Button } from "@chakra-ui/react"
import { useEffect, useState } from "react"
import { BlitzPage } from "blitz"
import MealSection from "app/core/components/MealSection"
import DashboardLayout from "app/core/layouts/DashboardLayout"
import Sidebar from "app/core/components/Sidebar"

const Meals: BlitzPage = () => {
  return (
    <Flex>
      <MealSection />
    </Flex>
  )
}

Meals.suppressFirstRenderFlicker = true
Meals.getLayout = (page) => {
  return <DashboardLayout title="Meals">{page}</DashboardLayout>
}

export default Meals
