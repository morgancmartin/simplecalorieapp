import { Flex, Button } from "@chakra-ui/react"
import { useEffect, useState } from "react"
import UserDashboard from "app/core/components/UserDashboard"
import AdminDashboard from "app/core/components/AdminDashboard"
import FoodEntrySection from "app/core/components/FoodEntrySection"
import { useCurrentUser } from "app/core/hooks/useCurrentUser"
import { BlitzPage, useRouter } from "blitz"
import { Link } from "blitz"
import Layout from "app/core/layouts/Layout"
import Sidebar from "app/core/components/Sidebar"

const Page: BlitzPage = () => {
  const router = useRouter()
  const currentUser = useCurrentUser()

  useEffect(() => {
    if (!currentUser) {
      router.push("/login")
    } else {
      router.push("/foodEntries")
    }
  }, [])

  return <></>
}

Page.suppressFirstRenderFlicker = true
Page.getLayout = (page) => {
  return <Layout title="Dashboard">{page}</Layout>
}

export default Page
