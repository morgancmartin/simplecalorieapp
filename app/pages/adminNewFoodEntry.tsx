import { Flex, Button } from "@chakra-ui/react"
import { useEffect, useState } from "react"
import { useMutation } from "blitz"
import { BlitzPage } from "blitz"
import { useCurrentUser } from "app/core/hooks/useCurrentUser"
import AdminFoodEntryForm from "app/core/components/AdminFoodEntryForm"
import DashboardLayout from "app/core/layouts/DashboardLayout"

const AdminNewFoodEntry: BlitzPage = () => {
  return <AdminFoodEntryForm />
}

AdminNewFoodEntry.suppressFirstRenderFlicker = true

AdminNewFoodEntry.getLayout = (page) => {
  return <DashboardLayout title="Meals">{page}</DashboardLayout>
}

export default AdminNewFoodEntry
