import { Flex, Button } from "@chakra-ui/react"
import { useEffect, useState } from "react"
import { BlitzPage } from "blitz"
import { useCurrentUser } from "app/core/hooks/useCurrentUser"
import FoodEntrySection from "app/core/components/FoodEntrySection"
import DashboardLayout from "app/core/layouts/DashboardLayout"

const FoodEntries: BlitzPage = () => {
  return <FoodEntrySection />
}

FoodEntries.suppressFirstRenderFlicker = true

FoodEntries.getLayout = (page) => {
  return <DashboardLayout title="Meals">{page}</DashboardLayout>
}

export default FoodEntries
