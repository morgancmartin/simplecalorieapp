import { Ctx } from "blitz"
import db from "db"
import * as z from "zod"

const GetFoodEntries = z.object({})

export default async function getFoodEntries(input: z.infer<typeof GetFoodEntries>, ctx: Ctx) {
  // Validate the input
  // const data = GetFoodEntries.parse(input)

  // Require user to be logged in
  ctx.session.$authorize()

  const user = await db.user.findUnique({ where: { id: ctx.session.userId } })
  const isAdmin = user?.role === "ADMIN"
  const foodEntries = await (isAdmin
    ? db.foodEntry.findMany({ include: { user: true } })
    : db.foodEntry.findMany({ where: { userId: ctx.session.userId } }))
  // .findOne({ where: { id: data.id } })

  // Can do any processing, fetching from other APIs, etc

  return foodEntries
}
