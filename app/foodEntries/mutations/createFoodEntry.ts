import { Ctx } from "blitz"
import db from "db"
import * as z from "zod"

export const CreateFoodEntry = z
  .object({
    name: z.string(),
    consumptionDate: z.date(),
    calories: z.number(),
    mealId: z.number(),
    userId: z.number().optional(),
  })
  .nonstrict()

export default async function createFoodEntry(input: z.infer<typeof CreateFoodEntry>, ctx: Ctx) {
  // Validate input - very important for security
  const data = {
    ...CreateFoodEntry.parse(input),
    userId: input.userId ?? ctx.session.userId,
  }

  // Require user to be logged in
  ctx.session.$authorize()

  const foodEntry = await db.foodEntry.create({ data })

  // Can do any processing, fetching from other APIs, etc

  return foodEntry
}
