import { Ctx } from "blitz"
import db from "db"
import * as z from "zod"

const DeleteFoodEntry = z
  .object({
    id: z.number(),
  })
  .nonstrict()

export default async function deleteFoodEntry(input: z.infer<typeof DeleteFoodEntry>, ctx: Ctx) {
  // Validate input - very important for security
  const where = {
    ...DeleteFoodEntry.parse(input),
  }

  // Require user to be logged in
  ctx.session.$authorize()

  const foodEntry = await db.foodEntry.delete({ where })

  // Can do any processing, fetching from other APIs, etc

  return foodEntry
}
