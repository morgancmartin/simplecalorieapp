import { Ctx } from "blitz"
import db from "db"
import * as z from "zod"
import { omit, pick } from "lodash"

const UpdateFoodEntry = z
  .object({
    name: z.string().optional(),
    consumptionDate: z.date().optional(),
    calories: z.number().optional(),
    mealId: z.number().optional(),
  })
  .nonstrict()

const Where = z.object({
  id: z.number(),
})

export default async function updateFoodEntry(
  input: z.infer<typeof UpdateFoodEntry> & z.infer<typeof Where>,
  ctx: Ctx
) {
  // Validate input - very important for security
  const data = {
    ...UpdateFoodEntry.parse(omit(input, ["id"])),
  }

  const where = {
    ...Where.parse(pick(input, ["id"])),
  }

  // Require user to be logged in
  ctx.session.$authorize()

  const foodEntry = await db.foodEntry.update({ where, data })

  // Can do any processing, fetching from other APIs, etc

  return foodEntry
}
