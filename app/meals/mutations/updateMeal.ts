import { Ctx } from "blitz"
import db from "db"
import * as z from "zod"
import { omit, pick } from "lodash"

const UpdateMeal = z
  .object({
    name: z.string().optional(),
    maxEntries: z.number().optional(),
  })
  .nonstrict()

const Where = z.object({
  id: z.number(),
})

export default async function updateMeal(
  input: z.infer<typeof UpdateMeal> & z.infer<typeof Where>,
  ctx: Ctx
) {
  // Validate input - very important for security
  const data = {
    ...UpdateMeal.parse(omit(input, ["id"])),
  }

  const where = {
    ...Where.parse(pick(input, ["id"])),
  }

  // Require user to be logged in
  ctx.session.$authorize()

  const meal = await db.meal.update({ where, data })

  // Can do any processing, fetching from other APIs, etc

  return meal
}
