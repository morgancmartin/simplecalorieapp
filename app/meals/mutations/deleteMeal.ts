import { Ctx } from "blitz"
import db from "db"
import * as z from "zod"

const DeleteMeal = z
  .object({
    id: z.number(),
  })
  .nonstrict()

export default async function deleteMeal(input: z.infer<typeof DeleteMeal>, ctx: Ctx) {
  // Validate input - very important for security
  const where = {
    ...DeleteMeal.parse(input),
  }

  // Require user to be logged in
  ctx.session.$authorize()

  const meal = await db.meal.delete({ where })

  // Can do any processing, fetching from other APIs, etc

  return meal
}
