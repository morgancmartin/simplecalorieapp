import { Ctx } from "blitz"
import db from "db"
import * as z from "zod"

export const CreateMeal = z
  .object({
    name: z.string(),
    maxEntries: z.number(),
  })
  .nonstrict()

export default async function createMeal(input: z.infer<typeof CreateMeal>, ctx: Ctx) {
  // Validate input - very important for security
  const data = {
    ...CreateMeal.parse(input),
    userId: ctx.session.userId,
  }

  // Require user to be logged in
  ctx.session.$authorize()

  const meal = await db.meal.create({ data })

  // Can do any processing, fetching from other APIs, etc

  return meal
}
