import { Ctx, AuthenticationError } from "blitz"
import db from "db"
import * as z from "zod"

const GetUsers = z.object({})

export default async function getUsers(input: z.infer<typeof GetUsers>, ctx: Ctx) {
  // Validate the input
  // const data = GetUsers.parse(input)

  // Require user to be logged in
  ctx.session.$authorize()
  const user = await db.user.findUnique({ where: { id: ctx.session.userId } })
  const isAdmin = user?.role === "ADMIN"

  if (!isAdmin) {
    throw new AuthenticationError()
  }

  const users = await db.user.findMany()

  return users
}
