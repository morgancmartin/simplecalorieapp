import { Ctx, AuthenticationError } from "blitz"
import db from "db"
import * as z from "zod"

const GetMealsForUser = z.object({
  userId: z.number(),
})

export default async function getMealsForUser(input: z.infer<typeof GetMealsForUser>, ctx: Ctx) {
  // Validate the input
  const data = GetMealsForUser.parse(input)

  // Require user to be logged in
  ctx.session.$authorize()
  const user = await db.user.findUnique({ where: { id: ctx.session.userId } })
  const isAdmin = user?.role === "ADMIN"

  if (!isAdmin) {
    throw new AuthenticationError()
  }

  const meals = await db.meal.findMany({
    where: data,
    include: { foodEntries: true },
  })

  return meals
}
