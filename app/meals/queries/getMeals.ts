import { Ctx } from "blitz"
import db from "db"
import * as z from "zod"

const GetMeals = z.object({})

export default async function getMeals(input: z.infer<typeof GetMeals>, ctx: Ctx) {
  // Validate the input
  // const data = GetMeals.parse(input)

  // Require user to be logged in
  ctx.session.$authorize()
  const user = await db.user.findUnique({ where: { id: ctx.session.userId } })
  const isAdmin = user?.role === "ADMIN"

  const meals = await (isAdmin
    ? db.meal.findMany({ include: { foodEntries: true } })
    : db.meal.findMany({ where: { userId: ctx.session.userId }, include: { foodEntries: true } }))

  return meals
}
