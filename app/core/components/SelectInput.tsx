import { useState } from "react"
import { Select, Flex } from "@chakra-ui/react"
import { useField } from "react-final-form"
import { FormControl, FormLabel } from "@chakra-ui/form-control"

interface Option {
  id: string | number
  value: string
  disabled?: boolean
}

interface SelectInputProps {
  name: string
  options: any[]
  placeholder?: string
  defaultValue?: any
  onUpdate?: (v: any) => void
}

export default function SelectInput({
  name,
  options,
  placeholder,
  defaultValue,
  onUpdate,
}: SelectInputProps) {
  const {
    input,
    meta: { dirty, error, submitError },
  } = useField(name, {
    parse: Number,
    defaultValue,
  })

  const cProps = { placeholder }

  const normalizedError = Array.isArray(error) ? error.join(", ") : error || submitError
  const onChange = (e) => {
    input.onChange(e)
    if (onUpdate) {
      onUpdate(e.target.value)
    }
  }

  return (
    <FormControl>
      <Select {...input} onChange={onChange} {...cProps}>
        {options.map((opt) => (
          <option value={opt.id} key={opt.id} disabled={opt.disabled}>
            {opt.label} {opt.disabled ? opt.disabledMessage || "" : ""}
          </option>
        ))}
      </Select>
      {normalizedError && dirty && (
        <Flex role="alert" color="red">
          {normalizedError}
        </Flex>
      )}
    </FormControl>
  )
}

export function RawSelectInput({
  options,
  placeholder,
  defaultValue,
  onUpdate,
}: Omit<SelectInputProps, "name">) {
  const [value, setValue] = useState(defaultValue ?? "")

  const updateValue = (v) => setValue(v)

  const cProps = { placeholder }
  const onChange = (e) => {
    updateValue(e.target.value)
    if (onUpdate) {
      onUpdate(e.target.value)
    }
  }

  return (
    <Select {...cProps} value={value} onChange={onChange}>
      {options.map((opt) => (
        <option value={opt.id} key={opt.id} disabled={opt.disabled}>
          {opt.label}
        </option>
      ))}
    </Select>
  )
}
