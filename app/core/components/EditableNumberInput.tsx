import { useState, useEffect } from "react"
import {
  Flex,
  Text,
  NumberInput as CNumberInput,
  NumberInputProps as CNumberInputProps,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
} from "@chakra-ui/react"

interface EditableNumberInputProps {
  defaultValue: number
  onUpdate: (v: number) => void
}

export default function EditableNumberInput({ defaultValue, onUpdate }: EditableNumberInputProps) {
  const [editing, setEditing] = useState(false)
  const toggleEdit = () => setEditing(!editing)
  const [value, setValue] = useState(defaultValue)

  const updateValue = (newVal) => {
    console.log("updating with newVal:", newVal)
    if (newVal >= 1) {
      setValue(newVal)
    }
  }

  const cProps = { defaultValue }

  useEffect(() => {
    if (!editing) {
      onUpdate(Number(value))
    }
  }, [editing])

  return (
    <Flex justify="flex-end">
      <Flex justify="flex-end" width="5rem">
        {!editing ? (
          <Text onClick={toggleEdit} cursor="pointer">
            {value}
          </Text>
        ) : (
          <CNumberInput {...cProps} value={value} onChange={updateValue} onBlur={toggleEdit}>
            <NumberInputField autoFocus />
            <NumberInputStepper>
              <NumberIncrementStepper />
              <NumberDecrementStepper />
            </NumberInputStepper>
          </CNumberInput>
        )}
      </Flex>
    </Flex>
  )
}
