import RDatePicker from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css"
import { useState } from "react"
import { useField } from "react-final-form"
import { FormControl, FormLabel } from "@chakra-ui/form-control"
import { Flex, Box } from "@chakra-ui/react"

interface DatePickerProps {
  name: string
}

export default function DatePicker({ name }: DatePickerProps) {
  const [date, setDate] = useState(new Date())
  const {
    input,
    meta: { touched, dirty, error, submitError },
  } = useField(name, {
    value: date,
  })

  const normalizedError = Array.isArray(error) ? error.join(", ") : error || submitError

  return (
    <FormControl>
      <Box border="1px solid black" borderRadius="3px" width="min-content">
        <RDatePicker {...input} selected={input.value} autoComplete="off" />
      </Box>
      {touched && normalizedError && dirty && (
        <Flex role="alert" color="red">
          {normalizedError}
        </Flex>
      )}
    </FormControl>
  )
}
