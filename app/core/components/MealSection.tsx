import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer,
  Flex,
  Button,
  Text,
} from "@chakra-ui/react"
import { useMutation, useQuery } from "blitz"
import getMeals from "app/meals/queries/getMeals"
import deleteMeal from "app/meals/mutations/deleteMeal"
import updateMeal from "app/meals/mutations/updateMeal"
import EditableNumberInput from "app/core/components/EditableNumberInput"
import EditableInput from "app/core/components/EditableInput"
import AlertDialog from "app/core/components/AlertDialog"

export default function MealSection() {
  const [meals, { refetch }] = useQuery(getMeals, {})

  return (
    <Flex direction="column">
      <Meals {...{ meals, refetch }} />
    </Flex>
  )
}

interface MealsProps {
  meals: any[]
  refetch: any
}

function Meals({ meals = [], refetch }: MealsProps) {
  const [deleteMealMutation] = useMutation(deleteMeal)
  const [updateMealMutation] = useMutation(updateMeal)
  const getMealDeleter = (id) => async () => {
    await deleteMealMutation({ id })
    refetch()
  }

  const getMealUpdater = (key, id) => async (data) => {
    await updateMealMutation({ id, [key]: data })
    refetch()
  }

  return (
    <Flex w="50vw" direction="column">
      <Text fontSize="1.5rem">Meals</Text>
      <TableContainer>
        <Table variant="simple">
          <Thead>
            <Tr>
              <Th width="15rem">Name</Th>
              <Th isNumeric width="15rem">
                Max Entries
              </Th>
              <Th width="15rem">Delete</Th>
            </Tr>
          </Thead>
          <Tbody>
            {meals.map((meal) => (
              <Tr key={meal.id}>
                <Td>
                  <EditableInput
                    defaultValue={meal.name}
                    onUpdate={getMealUpdater("name", meal.id)}
                  />
                </Td>
                <Td isNumeric>
                  <EditableNumberInput
                    defaultValue={meal.maxEntries}
                    onUpdate={getMealUpdater("maxEntries", meal.id)}
                  />
                </Td>
                <Td>
                  <AlertDialog buttonText="Delete" onConfirm={getMealDeleter(meal.id)} />
                </Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>
    </Flex>
  )
}
