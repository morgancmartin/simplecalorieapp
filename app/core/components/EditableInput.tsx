import { useState, useRef } from "react"
import {
  Editable as CEditable,
  EditableInput as CEditableInput,
  EditablePreview as CEditablePreview,
  EditableInputProps as CEditableInputProps,
} from "@chakra-ui/react"

interface EditableInputProps {
  onUpdate: (v: string) => void
}

export default function EditableInput({
  defaultValue,
  onUpdate,
}: EditableInputProps & Pick<CEditableInputProps, "defaultValue">) {
  const [value, setValue] = useState(defaultValue)
  const [innerValue, setInnerValue] = useState(defaultValue)
  const inputRef = useRef()

  const updateValue = (newVal) => {
    if (newVal) {
      setValue(newVal)
    } else {
      /* console.log(inputRef.current)
       * console.log("setting inputref to value:", value)
       * inputRef.current.value = value */
    }
  }

  const onChange = (e) => {
    if (e.target.value) {
      setInnerValue(e.target.value)
    }
  }

  return (
    <CEditable
      width="10rem"
      defaultValue={value as string}
      value={innerValue}
      onChange={(v) => updateValue(v)}
      onBlur={() => onUpdate(value as string)}
    >
      <CEditablePreview cursor="pointer" />
      <CEditableInput onChange={onChange} ref={inputRef} cursor="pointer" />
    </CEditable>
  )
}
