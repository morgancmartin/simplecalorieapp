import {
  Text,
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  TableCaption,
  TableContainer,
  Flex,
  Button,
} from "@chakra-ui/react"
import { useCurrentUser } from "app/core/hooks/useCurrentUser"
import { useIsAdmin } from "app/core/hooks/useIsAdmin"
import moment from "moment"
import { find, groupBy, sum, sortBy } from "lodash"
import { useMutation, useQuery } from "blitz"
import getFoodEntries from "app/foodEntries/queries/getFoodEntries"

export default function CalorieTotals() {
  const [foodEntries, { refetch }] = useQuery(getFoodEntries, {})
  const currentUser = useCurrentUser()
  const calorieTotals = calculateCalorieTotals(foodEntries)
  const isAdmin = useIsAdmin()

  const getCalorieStatus = (calories) => {
    const limit = currentUser.calorieLimit

    if (calories < limit) {
      return { label: "Under Limit", color: "green.500" }
    } else if (calories === limit) {
      return { label: "Met Limit", color: "yellow.500" }
    } else {
      return { label: `Exceeded Limit: ${limit}`, color: "red.500" }
    }
  }

  return (
    <Flex direction="column">
      <TableContainer>
        <Text fontSize="1.5rem">Calorie Totals</Text>
        <Table variant="simple">
          <Thead>
            <Tr>
              <Th>Date</Th>
              <Th isNumeric>Calories</Th>
              {!isAdmin ? <Th>Calorie Max Relation</Th> : <></>}
            </Tr>
          </Thead>
          <Tbody>
            {calorieTotals.map((calorieTotal) => (
              <Tr key={calorieTotal.id}>
                <Td>{moment(calorieTotal.day).format("MMMM Do YYYY, h:mm:ss a")}</Td>
                <Td isNumeric>{calorieTotal.calories}</Td>
                {!isAdmin ? (
                  <Td>
                    <Text color={getCalorieStatus(calorieTotal.calories).color}>
                      {getCalorieStatus(calorieTotal.calories).label}
                    </Text>
                  </Td>
                ) : (
                  <></>
                )}
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>
    </Flex>
  )
}

function calculateCalorieTotals(foodEntries) {
  const dayOfFoodEntry = (foodEntry) => moment(foodEntry.consumptionDate).startOf("day").format()
  const calTotalsByDay = Object.entries(groupBy(foodEntries, dayOfFoodEntry)).map((entry) => ({
    id: entry[0],
    day: entry[0],
    calories: sum(entry[1].map((foodEntry) => foodEntry.calories)),
  }))

  return sortBy(calTotalsByDay, "day")
}
