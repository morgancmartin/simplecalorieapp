import { Stat, StatLabel, StatNumber, StatGroup, Flex } from "@chakra-ui/react"

interface AverageCalsStatsProps {
  avgCalsPastSevenDays: number
}

export default function AverageCalsStats({ avgCalsPastSevenDays }: AverageCalsStatsProps) {
  return (
    <Stat>
      <StatLabel>Avg. Calories per User of last 7 days</StatLabel>
      <StatNumber>{avgCalsPastSevenDays}</StatNumber>
    </Stat>
  )
}
