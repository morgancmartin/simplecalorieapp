import { Stat, StatLabel, StatNumber, StatGroup } from "@chakra-ui/react"

interface AddedEntriesStatsProps {
  today: number
  pastSevenDays: number
  weekBefore: number
}

export default function AddedEntriesStats({
  today,
  pastSevenDays,
  weekBefore,
}: AddedEntriesStatsProps) {
  return (
    <StatGroup width="50vw">
      <Stat>
        <StatLabel>Entries today</StatLabel>
        <StatNumber>{today}</StatNumber>
      </Stat>
      <Stat>
        <StatLabel>Entries (past 7 days)</StatLabel>
        <StatNumber>{pastSevenDays}</StatNumber>
      </Stat>
      <Stat>
        <StatLabel>Entries (week before)</StatLabel>
        <StatNumber>{weekBefore}</StatNumber>
      </Stat>
    </StatGroup>
  )
}
