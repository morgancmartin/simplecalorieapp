import { Meal, FoodEntry } from "db"
import { Text, Flex } from "@chakra-ui/react"
import { useEffect, useState } from "react"
import { Form } from "app/core/components/Form"
import { useMutation, useQuery } from "blitz"
import createFoodEntry from "app/foodEntries/mutations/createFoodEntry"
import getFoodEntries from "app/foodEntries/queries/getFoodEntries"
import getMeals from "app/meals/queries/getMeals"
import moment from "moment"
import SelectInput, { RawSelectInput } from "app/core/components/SelectInput"
import { LabeledTextField } from "app/core/components/LabeledTextField"
import DatePicker from "app/core/components/DatePicker"
import "react-datepicker/dist/react-datepicker.css"
import TimePicker from "app/core/components/TimePicker"
import "rc-time-picker/assets/index.css"
import NumberInput from "app/core/components/NumberInput"
import { CreateFoodEntry } from "app/auth/validations"
import { Link } from "blitz"
import { useFormState } from "react-final-form"
import { atom, useAtom } from "jotai"

type Meals = Array<Meal & { foodEntries: FoodEntry[] }>

const onSubmitCallbacksAtom = atom<Array<Function>>([])

interface FoodEntryFormProps {}

export default function FoodEntryForm() {
  const [meals, { refetch }] = useQuery(getMeals, {})

  const initialValues = {
    name: "",
    consumptionDate: new Date(),
    consumptionTime: moment(),
    calories: 0,
    mealId: undefined,
  }

  const [createFoodEntryMutation] = useMutation(createFoodEntry)
  const noMeals = !meals.length
  const [onSubmitCallbacks] = useAtom(onSubmitCallbacksAtom)

  const onSubmit = async (values, form) => {
    values.consumptionDate = combineDateAndTime(values.consumptionDate, values.consumptionTime)
    delete values.consumptionTime
    form.reset()
    await createFoodEntryMutation(values)
    /* refetch() */
    onSubmitCallbacks.map((cb) => cb())
  }

  return (
    <Flex direction="column" width="20vw">
      {noMeals ? (
        <Text fontSize="1.5rem">
          You must first create a meal before creating a food entry. Click{" "}
          <Text color="blue" textDecor="underline" display="inline">
            <Link href="/newMeal">here</Link>
          </Text>{" "}
          to create a meal.
        </Text>
      ) : (
        <>
          <Text fontSize="1.5rem">Create Food Entry</Text>
          <Form
            submitText="Create Food Entry"
            schema={CreateFoodEntry}
            initialValues={initialValues}
            onSubmit={onSubmit}
          >
            <LabeledTextField name="name" label="Name" placeholder="Name" />
            <DatePicker name="consumptionDate" />
            <TimePicker name="consumptionTime" rcProps={{ showSecond: false, use12Hours: true }} />
            <NumberInput name="calories" step={10} defaultValue={0} />
            <MealSelector name="mealId" />
          </Form>
        </>
      )}
    </Flex>
  )
}

function combineDateAndTime(date, time) {
  return new Date(moment(date).hour(time.hour()).minute(time.minute()).format())
}

interface MealSelectorProps {
  name?: string
  defaultValue?: any
}

export function MealSelector({ name, defaultValue }: MealSelectorProps) {
  const [lastDate, setLastDate] = useState()
  const [meals, { refetch }] = useQuery(getMeals, {})
  const [onSubmitCallbacks, setOnSubmitCallbacks] = useAtom(onSubmitCallbacksAtom)

  const formState = useFormState({
    onChange: (formState) => {
      if (
        !moment(lastDate)
          .startOf("day")
          .isSame(moment(formState.values.consumptionDate).startOf("day"))
      ) {
        setLastDate(formState.values.consumptionDate)
      }
    },
  })

  const [options, setOptions] = useState([])

  const getFoodEntryDateFilter = (date) => (foodEntries) =>
    foodEntries.filter((foodEntry) =>
      moment(foodEntry.consumptionDate).startOf("day").isSame(moment(date).startOf("day"))
    )

  const generateOptions = (meals) => {
    const foodEntryDateFilter = getFoodEntryDateFilter(lastDate)
    const options = meals.map((meal) => {
      const filtered = foodEntryDateFilter(meal.foodEntries)
      console.log("FILTERED:", filtered)
      const disabled = filtered.length >= meal.maxEntries
      return {
        id: meal.id,
        label: meal.name,
        disabled,
        disabledMessage: "(Max entries for selected day is met)",
      }
    })
    setOptions(options)
  }

  const regenerateOptions = () => {
    refetch().then(({ data: meals }) => {
      generateOptions(meals)
    })
  }

  useEffect(() => {
    setOnSubmitCallbacks([...onSubmitCallbacks, regenerateOptions])
    onSubmitCallbacks.map((cb) => cb())
  }, [])

  useEffect(() => {
    generateOptions(meals)
  }, [formState.values])

  return (
    <SelectInput
      name={name as string}
      defaultValue={defaultValue}
      options={options}
      placeholder="Select meal"
    />
  )
}
