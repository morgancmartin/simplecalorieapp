import { useRef } from "react"
import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
  useDisclosure,
  Button,
} from "@chakra-ui/react"

interface MyAlertDialogProps {
  buttonText: string
  onConfirm: () => void
}

export default function MyAlertDialog({ buttonText, onConfirm }: MyAlertDialogProps) {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const cancelRef = useRef()

  const confirm = () => {
    onConfirm()
    onClose()
  }

  return (
    <>
      <Button colorScheme="red" onClick={onOpen}>
        {buttonText}
      </Button>
      <AlertDialog isOpen={isOpen} leastDestructiveRef={cancelRef as any} onClose={onClose}>
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Delete Customer
            </AlertDialogHeader>

            <AlertDialogBody>Are you sure? You can't undo this action afterwards.</AlertDialogBody>

            <AlertDialogFooter>
              <Button ref={cancelRef} onClick={onClose}>
                Cancel
              </Button>
              <Button colorScheme="red" onClick={confirm} ml={3}>
                Delete
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </>
  )
}
