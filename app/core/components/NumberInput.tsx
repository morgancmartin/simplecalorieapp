import {
  NumberInput as CNumberInput,
  NumberInputProps as CNumberInputProps,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  Flex,
} from "@chakra-ui/react"
import { useField } from "react-final-form"
import { FormControl, FormLabel } from "@chakra-ui/form-control"

interface NumberInputProps {
  name?: string
  step?: number
  defaultValue?: number
  label?: string
}

export default function NumberInput({
  name = "",
  step,
  defaultValue,
  as,
  label,
}: NumberInputProps & Pick<CNumberInputProps, "as">) {
  const {
    input,
    meta: { touched, dirty, error, submitError, submitting },
  } = useField(name, {
    parse: Number,
  })

  const onChange = (v) => {
    if (v >= 1 || v === "") {
      input.onChange(v === "" ? null : v)
    }
  }

  const normalizedError = Array.isArray(error) ? error.join(", ") : error || submitError

  const cProps = { step, defaultValue, as, onChange }

  return (
    <FormControl>
      {label && <FormLabel>{label}</FormLabel>}
      <CNumberInput {...input} {...cProps}>
        <NumberInputField />
        <NumberInputStepper>
          <NumberIncrementStepper />
          <NumberDecrementStepper />
        </NumberInputStepper>
      </CNumberInput>
      {normalizedError && dirty && (
        <Flex role="alert" color="red">
          {normalizedError}
        </Flex>
      )}
    </FormControl>
  )
}
