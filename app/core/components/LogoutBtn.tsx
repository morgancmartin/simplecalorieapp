import logout from "app/auth/mutations/logout"
import { Button } from "@chakra-ui/react"
import { useMutation, useRouter } from "blitz"

export default function LogoutBtn() {
  const router = useRouter()
  const [logoutMutation] = useMutation(logout)

  return (
    <Button
      onClick={async () => {
        await logoutMutation()
        router.push("/login")
      }}
    >
      Logout
    </Button>
  )
}
