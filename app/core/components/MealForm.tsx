import { Flex, Text } from "@chakra-ui/react"
import { useMutation } from "blitz"
import createMeal from "app/meals/mutations/createMeal"
import { Form } from "app/core/components/Form"
import { LabeledTextField } from "app/core/components/LabeledTextField"
import NumberInput from "app/core/components/NumberInput"
import { CreateMeal } from "app/auth/validations"

interface MealFormProps {}

export default function MealForm() {
  const [createMealMutation] = useMutation(createMeal)
  const createMealForVals = async (values, form) => {
    form.reset()
    await createMealMutation(values)
    /* refetch() */
  }

  return (
    <Flex w="50vw" direction="column">
      <Text fontSize="1.5rem">Create Meal</Text>
      <Form
        submitText="Create Meal"
        schema={CreateMeal}
        initialValues={{ name: "", maxEntries: 1 }}
        onSubmit={createMealForVals}
      >
        <LabeledTextField name="name" label="Name" placeholder="Name" />
        <NumberInput name="maxEntries" defaultValue={1} />
      </Form>
    </Flex>
  )
}
