import { useEffect, useState } from "react"
import { useQuery } from "blitz"
import { useIsAdmin } from "app/core/hooks/useIsAdmin"
import getUsers from "app/meals/queries/getUsers"
import SelectInput, { RawSelectInput } from "app/core/components/SelectInput"
import { atom, useAtom } from "jotai"

interface UserSelectorProps {
  name?: string
  defaultValue?: any
  onUpdate?: (v: any) => void
}

export const userIdAtom = atom<number>()

export default function UserSelector({ name, defaultValue }: UserSelectorProps) {
  const isAdmin = useIsAdmin()
  const [users, { refetch }] = useQuery(getUsers, {})
  const [options, setOptions] = useState([])
  const [lastDate, setLastDate] = useState()
  const [userId, setUserId] = useAtom(userIdAtom)
  const onUserUpdate = (userId) => setUserId(Number(userId))

  useEffect(() => {
    setOptions(
      users
        .filter((user) => user.role !== "ADMIN")
        .map((user) => ({ id: user.id, label: user.email }))
    )
  }, [users])

  return name ? (
    <SelectInput
      name={name}
      defaultValue={defaultValue}
      options={options}
      placeholder="Select user"
      onUpdate={onUserUpdate}
    />
  ) : (
    <RawSelectInput
      defaultValue={defaultValue}
      options={options}
      placeholder="Select user"
      onUpdate={onUserUpdate}
    />
  )
}
