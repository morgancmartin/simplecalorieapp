import RCTimePicker, { TimePickerProps as RCTimePickerProps } from "rc-time-picker"
import { useState } from "react"
import { useField } from "react-final-form"
import moment, { Moment } from "moment"
import { FormControl, FormLabel } from "@chakra-ui/form-control"
import { Flex, Box } from "@chakra-ui/react"

interface TimePickerProps {
  name: string
  rcProps: RCTimePickerProps
}

export default function TimePicker({ name, rcProps }: TimePickerProps) {
  const {
    input,
    meta: { touched, dirty, error, submitError },
  } = useField(name, {
    value: moment(),
  })

  const normalizedError = Array.isArray(error) ? error.join(", ") : error || submitError

  return (
    <FormControl>
      <RCTimePicker {...input} {...rcProps} />
      {normalizedError && (
        <Flex role="alert" color="red">
          {normalizedError}
        </Flex>
      )}
    </FormControl>
  )
}
