import {
  TableContainer,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  Flex,
  Button,
  Box,
  Text,
} from "@chakra-ui/react"
import { useMutation, useQuery } from "blitz"
import { useEffect, useState } from "react"
import getFoodEntries from "app/foodEntries/queries/getFoodEntries"
import getMeals from "app/meals/queries/getMeals"
import { find, sortBy } from "lodash"
import moment from "moment"
import deleteFoodEntry from "app/foodEntries/mutations/deleteFoodEntry"
import RDatePicker from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css"
import EditableInput from "app/core/components/EditableInput"
import EditableNumberInput from "app/core/components/EditableNumberInput"
import updateFoodEntry from "app/foodEntries/mutations/updateFoodEntry"
import { MealSelector } from "app/core/components/FoodEntryForm"
import AlertDialog from "app/core/components/AlertDialog"
import { useIsAdmin } from "app/core/hooks/useIsAdmin"
import { FoodEntry } from "db"
import UserSelector, { userIdAtom } from "app/core/components/UserSelector"
import { useAtom } from "jotai"

export default function FoodEntrySection() {
  const [foodEntries, { refetch }] = useQuery(getFoodEntries, {})

  return (
    <Flex direction="column">
      <Text fontSize="1.5rem">Food Entries</Text>
      <Flex>
        <FoodEntries {...{ foodEntries, refetch }} />
      </Flex>
    </Flex>
  )
}

interface FoodEntriesProps {
  foodEntries: FoodEntry[]
  refetch: any
}

function FoodEntries({ foodEntries, refetch }: FoodEntriesProps) {
  const isAdmin = useIsAdmin()
  const [filteredFoodEntries, setFilteredFoodEntries] = useState<FoodEntry[]>([])
  const [meals] = useQuery(getMeals, {})
  const mealById = (id: number) => find(meals, { id })
  const [fromDate, setFromDate] = useState(today())
  const [toDate, setToDate] = useState(today())
  const updateFromDate = (v) => setFromDate(v)
  const updateToDate = (v) => setToDate(v)
  const [updateFoodEntryMutation] = useMutation(updateFoodEntry)
  const [userId] = useAtom(userIdAtom)

  const getFoodEntryUpdater = (key, id) => async (data) => {
    await updateFoodEntryMutation({ id, [key]: data })
    refetch()
  }

  useEffect(() => {
    let filtered = foodEntries.filter(
      (foodEntry) =>
        moment(foodEntry.consumptionDate).startOf("day").isSameOrAfter(fromDate) &&
        moment(foodEntry.consumptionDate).startOf("day").isSameOrBefore(toDate)
    )
    if (isAdmin) {
      filtered = filtered.filter((foodEntry) => foodEntry.userId === userId)
    }
    setFilteredFoodEntries(sortBy<FoodEntry>(filtered, "consumptionDate"))
  }, [foodEntries, fromDate, toDate, userId])

  const getFoodEntryDeleter = (id: number) => async () => {
    await deleteFoodEntry({ id })
    refetch()
  }

  return (
    <Flex direction="column">
      <Flex>
        <Flex width="6rem">
          <RDatePicker
            selected={fromDate}
            onChange={updateFromDate}
            selectsStart
            startDate={fromDate}
            endDate={toDate}
          />
        </Flex>
        <Flex width="6rem">
          <RDatePicker
            selected={toDate}
            onChange={updateToDate}
            selectsEnd
            startDate={fromDate}
            endDate={toDate}
          />
        </Flex>
        {!isAdmin ? (
          <></>
        ) : (
          <Flex>
            <UserSelector defaultValue={userId} />
          </Flex>
        )}
      </Flex>
      <TableContainer>
        <Table variant="simple">
          <Thead>
            <Tr>
              {isAdmin && <Th>User Email</Th>}
              <Th>Name</Th>
              <Th>Date</Th>
              <Th isNumeric>Calories</Th>
              <Th>Meal</Th>
              <Th>Delete</Th>
            </Tr>
          </Thead>
          <Tbody>
            {filteredFoodEntries.map((foodEntry) => (
              <Tr key={foodEntry.id}>
                {isAdmin && <Td>{foodEntry?.user?.email}</Td>}
                <Td>
                  <EditableInput
                    defaultValue={foodEntry.name}
                    onUpdate={getFoodEntryUpdater("name", foodEntry.id)}
                  />
                </Td>
                <Td>{moment(foodEntry.consumptionDate).format("MMMM Do YYYY, h:mm:ss a")}</Td>
                <Td isNumeric>
                  <EditableNumberInput
                    defaultValue={foodEntry.calories}
                    onUpdate={getFoodEntryUpdater("calories", foodEntry.id)}
                  />
                </Td>
                <Td>{mealById(foodEntry.mealId)?.name}</Td>
                <Td>
                  <AlertDialog buttonText="Delete" onConfirm={getFoodEntryDeleter(foodEntry.id)} />
                </Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>
    </Flex>
  )
}

interface DisplayInputToggleProps {
  displayValue: any
  Input: any
}

function DisplayInputToggle({ displayValue, Input }: DisplayInputToggleProps) {
  const [editing, setEditing] = useState(false)
  const toggle = () => setEditing(!editing)

  return !editing ? <Text onClick={toggle}>{displayValue}</Text> : <>{Input}</>
}

function yesterday() {
  return new Date(moment().startOf("day").subtract(1, "days").format())
}

function today() {
  return new Date(moment().startOf("day").format())
}
