import { Flex, Button } from "@chakra-ui/react"
import { useEffect } from "react"
import { Link } from "blitz"
import { useRouter } from "blitz"
import { useIsAdmin } from "app/core/hooks/useIsAdmin"

export default function Sidebar() {
  const isAdmin = useIsAdmin()

  return (
    <Flex width="10vw" height="100vh" direction="column" backgroundColor="#F0F0F0" padding="4px">
      <SidebarItem label="All Food Entries" path="/foodEntries" />
      <SidebarItem label="New Food Entries" path="/newFoodEntry" adminPath="/adminNewFoodEntry" />
      {!isAdmin && (
        <>
          <SidebarItem label="Meals" path="/meals" />
          <SidebarItem label="New Meal" path="/newMeal" />
          <SidebarItem label="Calorie Totals" path="/calorieTotals" />
        </>
      )}
      {isAdmin && <SidebarItem label="Report" path="/report" />}
    </Flex>
  )
}

interface SidebarItemProps {
  label: string
  path: string
  adminPath?: string
}

function SidebarItem({ label, path, adminPath }: SidebarItemProps) {
  const router = useRouter()
  const isCurrent = (path) => router.pathname === path
  const isAdmin = useIsAdmin()
  if (!adminPath) {
    adminPath = path
  }
  const href = isAdmin ? adminPath : path

  return (
    <Flex justifyContent="center" marginBottom="5px">
      <Link href={href}>
        <Button
          w="100%"
          backgroundColor={isCurrent(href) ? "#E0E0E5" : ""}
          fontSize="14px"
          fontWeight={isCurrent(href) ? "bold" : "normal"}
        >
          {label}
        </Button>
      </Link>
    </Flex>
  )
}
