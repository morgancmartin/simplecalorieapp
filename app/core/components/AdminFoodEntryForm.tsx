import { Meal, FoodEntry } from "db"
import { Text, Flex } from "@chakra-ui/react"
import { useEffect, useState } from "react"
import { Form } from "app/core/components/Form"
import { useMutation, useQuery } from "blitz"
import createFoodEntry from "app/foodEntries/mutations/createFoodEntry"
import getFoodEntries from "app/foodEntries/queries/getFoodEntries"
import getUsers from "app/meals/queries/getUsers"
import getMealsForUser from "app/meals/queries/getMealsForUser"
import moment from "moment"
import SelectInput, { RawSelectInput } from "app/core/components/SelectInput"
import { LabeledTextField } from "app/core/components/LabeledTextField"
import DatePicker from "app/core/components/DatePicker"
import "react-datepicker/dist/react-datepicker.css"
import TimePicker from "app/core/components/TimePicker"
import "rc-time-picker/assets/index.css"
import NumberInput from "app/core/components/NumberInput"
import { CreateFoodEntry } from "app/auth/validations"
import { Link } from "blitz"
import { useFormState } from "react-final-form"
import { useIsAdmin } from "app/core/hooks/useIsAdmin"
import { useAtom, atom } from "jotai"
import UserSelector, { userIdAtom } from "app/core/components/UserSelector"

type Meals = Array<Meal & { foodEntries: FoodEntry[] }>

const onSubmitCallbacksAtom = atom<Array<Function>>([])

export default function AdminFoodEntryForm() {
  const initialValues = {
    name: "",
    consumptionDate: new Date(),
    consumptionTime: moment(),
    calories: 0,
    mealId: undefined,
    userId: undefined,
  }

  const [createFoodEntryMutation] = useMutation(createFoodEntry)
  const [onSubmitCallbacks] = useAtom(onSubmitCallbacksAtom)

  const onSubmit = async (values, form) => {
    values.consumptionDate = combineDateAndTime(values.consumptionDate, values.consumptionTime)
    delete values.consumptionTime
    form.reset()
    await createFoodEntryMutation(values)
    /* refetch() */
    onSubmitCallbacks.map((cb) => cb())
  }

  return (
    <Flex direction="column" width="20vw">
      <Text fontSize="1.5rem">Create Food Entry</Text>
      <Form
        submitText="Create Food Entry"
        schema={CreateFoodEntry}
        initialValues={initialValues}
        onSubmit={onSubmit}
      >
        <LabeledTextField name="name" label="Name" placeholder="Name" />
        <UserSelector name="userId" />
        <DatePicker name="consumptionDate" />
        <TimePicker name="consumptionTime" rcProps={{ showSecond: false, use12Hours: true }} />
        <NumberInput name="calories" step={10} defaultValue={0} />
        <AdminMealSelector name="mealId" />
      </Form>
    </Flex>
  )
}

function combineDateAndTime(date, time) {
  return new Date(moment(date).hour(time.hour()).minute(time.minute()).format())
}

interface AdminMealSelectorProps {
  name: string
}

function AdminMealSelector({ name }: AdminMealSelectorProps) {
  const [lastDate, setLastDate] = useState()
  const [foodEntries] = useQuery(getFoodEntries, {})
  const [userId] = useAtom(userIdAtom)
  const [meals, { refetch }] = useQuery(
    getMealsForUser,
    { userId },
    { enabled: Boolean(userId !== null && userId !== undefined) }
  )
  const [onSubmitCallbacks, setOnSubmitCallbacks] = useAtom(onSubmitCallbacksAtom)

  const formState = useFormState({
    onChange: (formState) => {
      if (
        !moment(lastDate)
          .startOf("day")
          .isSame(moment(formState.values.consumptionDate).startOf("day"))
      ) {
        setLastDate(formState.values.consumptionDate)
      }
    },
  })

  useEffect(() => {
    if (userId) {
      refetch()
    }
  }, [userId])

  const [options, setOptions] = useState([])

  const getFoodEntryDateFilter = (date) => (foodEntries) =>
    foodEntries.filter((foodEntry) =>
      moment(foodEntry.consumptionDate).startOf("day").isSame(moment(date).startOf("day"))
    )

  const generateOptions = (meals) => {
    const foodEntryDateFilter = getFoodEntryDateFilter(lastDate)

    const options = meals.map((meal) => {
      const disabled = foodEntryDateFilter(meal.foodEntries).length >= meal.maxEntries
      return {
        id: meal.id,
        label: meal.name,
        disabled,
        disabledMessage: "(Max entries for selected day is met)",
      }
    })
    setOptions(options)
  }

  const regenerateOptions = () => {
    refetch().then(({ data: meals }) => {
      generateOptions(meals)
    })
  }

  useEffect(() => {
    setOnSubmitCallbacks([...onSubmitCallbacks, regenerateOptions])
  }, [])

  useEffect(() => {
    if (meals && meals.length) {
      generateOptions(meals)
    }
  }, [formState.values])

  return <SelectInput name={name} options={options} placeholder="Select meal" />
}
