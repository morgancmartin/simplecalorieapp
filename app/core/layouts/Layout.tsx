import { useState } from "react"
import { Flex, Button } from "@chakra-ui/react"
import { Head, BlitzLayout } from "blitz"
import LogoutBtn from "app/core/components/LogoutBtn"
import CalorieTotals from "app/core/components/CalorieTotals"
import Sidebar from "app/core/components/Sidebar"

const Layout: BlitzLayout<{
  title?: string
  children?: React.ReactNode
}> = ({ title, children }) => {
  return (
    <>
      <Head>
        <title>{title || "toptalSimpleCalorieApp"}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Flex width="100vw">
        <Flex justifyContent="center" alignItems="center" width="100%">
          {children}
        </Flex>
      </Flex>
    </>
  )
}

export default Layout
