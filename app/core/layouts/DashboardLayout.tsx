import { useState } from "react"
import { Flex, Button } from "@chakra-ui/react"
import { Head, BlitzLayout } from "blitz"
import LogoutBtn from "app/core/components/LogoutBtn"
import CalorieTotals from "app/core/components/CalorieTotals"
import Sidebar from "app/core/components/Sidebar"

const DashboardLayout: BlitzLayout<{
  title?: string
  children?: React.ReactNode
}> = ({ title, children }) => {
  const [mode, setMode] = useState("foodEntries")
  const getModeUpdater = (mode) => () => setMode(mode)

  return (
    <>
      <Head>
        <title>{title || "toptalSimpleCalorieApp"}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Flex width="100vw" maxW="99vw" overflowX="hidden" justifyContent="center">
        <Sidebar />
        <Flex direction="column" width="100%">
          <Flex
            justifyContent="flex-end"
            height="60px"
            alignItems="center"
            borderBottom="1px solid rgb(209, 212, 215)"
            paddingRight="1rem"
          >
            <LogoutBtn />
          </Flex>
          <Flex
            direction="column"
            width="100%"
            padding="2rem"
            alignItems="center"
            height="calc(100vh - 60px)"
            overflowY="auto"
          >
            {children}
          </Flex>
        </Flex>
      </Flex>
    </>
  )
}

export default DashboardLayout
