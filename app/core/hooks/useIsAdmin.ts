import { useCurrentUser } from "./useCurrentUser"

export const useIsAdmin = () => {
  const currentUser = useCurrentUser()

  return currentUser?.role === "ADMIN"
}
