import db, { User, Meal } from "./index"
import { Role } from "types"
import { SecurePassword } from "blitz"
import { CreateFoodEntry } from "app/foodEntries/mutations/createFoodEntry"
import { CreateMeal } from "app/meals/mutations/createMeal"
import * as z from "zod"
import moment from "moment"
import ch from "chance"

const chance = new ch()

/*
 * This seed function is executed when you run `blitz db seed`.
 *
 * Probably you want to use a library like https://chancejs.com
 * to easily generate realistic data.
 */
const seed = async () => {
  // await db.user.create({ data: { name: "Project " + i } })

  const adminOne = await createUser({
    email: "adminOne@gmail.com",
    password: "mypassword",
    role: "ADMIN",
    calorieLimit: 2100,
  })

  const userOne = await createUser({
    email: "userOne@gmail.com",
    password: "mypassword",
    role: "USER",
    calorieLimit: 2100,
  })

  const userTwo = await createUser({
    email: "userTwo@gmail.com",
    password: "mypassword",
    role: "USER",
    calorieLimit: 2100,
  })

  const userOneMeals = await createMealsForUser(userOne)
  createFoodEntriesForUser({
    user: userOne,
    meals: userOneMeals,
  })

  const userTwoMeals = await createMealsForUser(userTwo)
  createFoodEntriesForUser({
    user: userTwo,
    meals: userTwoMeals,
  })
}

async function createUser({
  email,
  password,
  role,
  calorieLimit,
}: {
  email: string
  password: string
  role: Role
  calorieLimit: number
}) {
  const hashedPassword = await SecurePassword.hash(password.trim())
  return await db.user.create({
    data: { email: email.toLowerCase().trim(), hashedPassword, role, calorieLimit },
    select: { id: true, name: true, email: true, role: true, calorieLimit: true },
  })
}

async function createMealsForUser({ id: userId }: Pick<User, "id">) {
  const breakfast = await createMeal({
    name: "Breakfast",
    maxEntries: 3,
    userId,
  })

  const lunch = await createMeal({
    name: "Lunch",
    maxEntries: 3,
    userId,
  })

  const dinner = await createMeal({
    name: "Dinner",
    maxEntries: 3,
    userId,
  })

  return {
    breakfast,
    lunch,
    dinner,
  }
}

async function createFoodEntriesForUser({
  user,
  meals,
}: {
  user: Omit<User, "hashedPassword" | "createdAt" | "updatedAt">
  meals: Record<string, Meal>
}) {
  ;[...Array(30).keys()].forEach(async (daysAgo) => {
    Object.values(meals).forEach(async (meal) => {
      const foods = chance
        .shuffle(foodsByType[meal.name?.toLowerCase() as string])
        .slice(0, Math.floor(Math.random() * meal.maxEntries))
      foods.forEach(async (food) => {
        await createFoodEntry({
          name: food.name,
          consumptionDate: getDateTimeByMealType({
            daysAgo,
            mealName: meal.name?.toLowerCase() as MealName,
          }),
          calories: Math.floor(food.calories * 1.3),
          mealId: meal.id,
          userId: user.id,
        })
      })
    })
  })
}

const foodsByType = {
  breakfast: [
    { name: "Bacon", calories: 300 },
    { name: "Eggs", calories: 300 },
    { name: "Orange Juice", calories: 100 },
    { name: "Cereal", calories: 250 },
  ],
  lunch: [
    { name: "Burger", calories: 450 },
    { name: "Fries", calories: 350 },
    { name: "Milkshake", calories: 400 },
    { name: "Sandwich", calories: 300 },
    { name: "Pizza", calories: 400 },
  ],
  dinner: [
    { name: "Spaghetti", calories: 400 },
    { name: "Pork", calories: 350 },
    { name: "Potatoes", calories: 300 },
    { name: "Curry", calories: 400 },
    { name: "Beef", calories: 300 },
  ],
}

type MealName = "breakfast" | "lunch" | "dinner"

function getDateTimeByMealType({
  daysAgo,
  mealName,
}: {
  daysAgo: number
  mealName: MealName
}): Date {
  return {
    breakfast: moment().startOf("day").subtract(daysAgo, "days").add(6, "hours").toDate(),
    lunch: moment().startOf("day").subtract(daysAgo, "days").add(12, "hours").toDate(),
    dinner: moment().startOf("day").subtract(daysAgo, "days").add(18, "hours").toDate(),
  }[mealName]
}

async function createMeal(data: z.infer<typeof CreateMeal> & { userId: number }) {
  return await db.meal.create({ data })
}

async function createFoodEntry(data: z.infer<typeof CreateFoodEntry> & { userId: number }) {
  return await db.foodEntry.create({ data })
}

export default seed
